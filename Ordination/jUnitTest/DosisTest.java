package jUnitTest;

import static org.junit.Assert.*;
import java.time.LocalTime;
import org.junit.Test;
import ordination.Dosis;

public class DosisTest {

    //Test af Constructor
    @Test
    public void testDosis() {
        Dosis dosis = new Dosis(LocalTime.of(12, 00), 5);
        assertEquals(5, dosis.getAntal(), 0);
        assertEquals(LocalTime.of(12, 00), dosis.getTid());
    }
    
}
