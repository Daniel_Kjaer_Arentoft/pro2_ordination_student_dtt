package jUnitTest;

import static org.junit.Assert.*;
import org.junit.Test;
import ordination.Laegemiddel;

public class LaegemiddelTest {
    
    //Test af Constructor
    @Test
    public void test() {
        Laegemiddel laegemiddel = new Laegemiddel("LM1", 0.13, 0.20, 0.30, "Pille");
        assertEquals("LM1", laegemiddel.getNavn());
        assertEquals(0.13, laegemiddel.getEnhedPrKgPrDoegnLet(), 0.01);
        assertEquals(0.20, laegemiddel.getEnhedPrKgPrDoegnNormal(), 0.01);
        assertEquals(0.30, laegemiddel.getEnhedPrKgPrDoegnTung(), 0.01);
        assertEquals("Pille", laegemiddel.getEnhed());
    }
}
