package jUnitTest;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {
	
	private Laegemiddel lm;
	@Before
	public void setUp() throws Exception {
		lm = new Laegemiddel("LM1", 10, 15, 20, "pille");
	}

	@Test
	public void test() {
		// Constructor
		// TC1
		PN pn1 = new PN(lm, 5, LocalDate.of(2017, 2, 28), LocalDate.of(2017, 3, 28));
		assertEquals(lm, pn1.getLaegemiddel());
		assertEquals(5, pn1.getAntalEnheder(),0);
		assertEquals(LocalDate.of(2017, 2, 28), LocalDate.of(2017, 2, 28));
		assertEquals(LocalDate.of(2017, 3, 28), LocalDate.of(2017, 3, 28));
		// givDosis()
		PN pn2 = new PN(lm, 5, LocalDate.of(2017, 2, 27), LocalDate.of(2017, 3, 2));
		// TC1
		assertEquals(true, pn2.givDosis(LocalDate.of(2017, 2, 28)));
		// TC2
		assertEquals(true, pn2.givDosis(LocalDate.of(2017, 3, 1)));
		// TC3
		assertEquals(false, pn2.givDosis(LocalDate.of(2017, 2, 25)));
		// getAntalGangeGivet()
		PN pn3 = new PN(lm, 5, LocalDate.of(2017, 2, 27), LocalDate.of(2017, 3, 2));
		// TC1
		pn3.givDosis(LocalDate.of(2017, 2, 28));
		assertEquals(1, pn3.getAntalGangeGivet(),0);
		// TC2
		PN pn4 = new PN(lm, 5, LocalDate.of(2017, 2, 27), LocalDate.of(2017, 3, 10));
		pn4.givDosis(LocalDate.of(2017, 2, 28));
		pn4.givDosis(LocalDate.of(2017, 3, 1));
		pn4.givDosis(LocalDate.of(2017, 3, 1));
		pn4.givDosis(LocalDate.of(2017, 3, 1));
		pn4.givDosis(LocalDate.of(2017, 3, 1));
		assertEquals(5, pn4.getAntalGangeGivet(),0);
		// doegnDosis()
		// TC1
		PN pn5 = new PN(lm, 10, LocalDate.of(2017, 2, 27), LocalDate.of(2017, 3, 28));
		pn5.givDosis(LocalDate.of(2017, 2, 28));
		pn5.givDosis(LocalDate.of(2017, 3, 1));
		pn5.givDosis(LocalDate.of(2017, 3, 1));
		pn5.givDosis(LocalDate.of(2017, 3, 3));
		pn5.givDosis(LocalDate.of(2017, 3, 4));
		assertEquals(10, pn5.doegnDosis(), 0);
		// TC2
		PN pn = new PN(lm, 10, LocalDate.of(2017, 2, 27), LocalDate.of(2017, 3, 28));
		assertEquals(0, pn.doegnDosis(), 0);
		// TC3
		pn = new PN(lm, 4, LocalDate.of(2017, 2, 27), LocalDate.of(2017, 3, 28));
		pn.givDosis(LocalDate.of(2017, 2, 28));
		assertEquals(4, pn.doegnDosis(), 0);
		// samletDosis()
		// TC1
		pn = new PN(lm, 5, LocalDate.of(2017, 2, 28), LocalDate.of(2017, 3, 4));
		pn.givDosis(LocalDate.of(2017, 2, 28));
		pn.givDosis(LocalDate.of(2017, 3, 1));
		pn.givDosis(LocalDate.of(2017, 3, 2));
		pn.givDosis(LocalDate.of(2017, 3, 3));
		pn.givDosis(LocalDate.of(2017, 3, 4));
		assertEquals(25, pn.samletDosis(), 0);
		// TC2
		pn = new PN(lm, 5, LocalDate.of(2017, 2, 28), LocalDate.of(2017, 3, 4));
		assertEquals(0, pn.samletDosis(), 0);
		// TC3
		pn = new PN(lm, 2, LocalDate.of(2017, 2, 28), LocalDate.of(2017, 3, 4));
		pn.givDosis(LocalDate.of(2017, 2, 28));
		pn.givDosis(LocalDate.of(2017, 3, 1));
		pn.givDosis(LocalDate.of(2017, 3, 3));
		assertEquals(6, pn.samletDosis(), 1);
	}
}
