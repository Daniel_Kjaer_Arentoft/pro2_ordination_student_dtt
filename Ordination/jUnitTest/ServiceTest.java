package jUnitTest;

import static org.junit.Assert.*;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;
import storage.Storage;

public class ServiceTest {
	
	private Laegemiddel laegemiddel;
	private Patient patient;
	private Service s;
	private Laegemiddel f;

	@Before
	public void setUp() throws Exception {
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "pille");
		patient = new Patient("0110641522", "Ulla Nielsen", 59.9);
		f = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		s = Service.getTestService();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void opretPNOrdinationTestIllegal() {
		// TC1
		PN pn = s.opretPNOrdination(LocalDate.of(2017, 2, 3), LocalDate.of(2017, 2, 2), patient, laegemiddel, 1);
		assertEquals(pn, patient.getOrdinationer().get(patient.getOrdinationer().size() - 1));
	}
	
	@Test()
	public void opretPNOrdinationTest() {
		// TC2
		s.opretPNOrdination(LocalDate.of(2017, 2, 3), LocalDate.of(2017, 2, 13), patient, laegemiddel, 3);
	}
	
	@Test(expected = DateTimeException.class)
	public void opretPNOrdinationTestDateTime() {
		// TC3
		s.opretPNOrdination(LocalDate.of(2017, 13, 3), LocalDate.of(2017, 2, 3), patient, laegemiddel, 2);
		// TC4
		s.opretPNOrdination(LocalDate.of(2017, 2, 3), LocalDate.of(2017, 13, 3), patient, laegemiddel, 3);
	}
	
	@Test
	public void opretDagligFastOrdinationTest() {
		// TC1
		DagligFast df = s.opretDagligFastOrdination(LocalDate.of(2017, 2, 28), LocalDate.of(2017, 3, 10), patient, laegemiddel, 4, 2, 0, 4);
		assertEquals(df, patient.getOrdinationer().get(patient.getOrdinationer().size() - 1));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void opretDagligFastOrdinationTestIllegal() {
		// TC2
		s.opretDagligFastOrdination(LocalDate.of(2017, 2, 3), LocalDate.of(2017, 2, 2), patient, laegemiddel, 4, 2, 0, 4);
	}
	
	@Test(expected = DateTimeException.class)
	public void opretDagligFastOrdinationTestDate() {
		// TC3 
		s.opretDagligFastOrdination(LocalDate.of(2017, 13, 3), LocalDate.of(2017, 2, 3), patient, laegemiddel, 4, 2, 0, 4);
		// TC4
		s.opretDagligFastOrdination(LocalDate.of(2017, 2, 3), LocalDate.of(2017, 13, 3), patient, laegemiddel, 4, 2, 0, 4);
	}
	
	@Test
	public void opretDagligSkaevOrdinationTest() {
		// TC1
		DagligSkaev ds = s.opretDagligSkaevOrdination(LocalDate.of(2017, 3, 1), LocalDate.of(2017, 3, 28), patient, laegemiddel, new LocalTime[] { LocalTime.of(9, 30), LocalTime.of(16, 20) }, new double[] { 1, 3 });
		assertEquals(ds, patient.getOrdinationer().get(0));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void opretDagligSkaevOrdinationTestIllegal() {
		// TC2
		s.opretDagligSkaevOrdination(LocalDate.of(2017, 3, 1), LocalDate.of(2017, 3, 28), patient, laegemiddel, new LocalTime[] { LocalTime.of(9, 30), LocalTime.of(16, 20) }, new double[] { 1, 3, 1, 6 });
	
		// TC3
		s.opretDagligSkaevOrdination(LocalDate.of(2017, 3, 13), LocalDate.of(2017, 3, 8), patient, laegemiddel, new LocalTime[] { LocalTime.of(9, 30), LocalTime.of(16, 20) }, new double[] { 1, 3 });
	}
	
	@Test
	public void ordinationPNAnvendtTest() {
		// TC1
		PN pn = new PN(laegemiddel, 5, LocalDate.of(2017, 2, 13), LocalDate.of(2017, 2, 15));
		s.ordinationPNAnvendt(pn, LocalDate.of(2017, 2, 14));
		assertEquals(1, pn.getAntalGangeGivet(), 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void ordinationPNAnvendtTestIllegal() {
		// TC2
		PN pn = new PN(laegemiddel, 5, LocalDate.of(2017, 2, 13), LocalDate.of(2017, 2, 15));
		s.ordinationPNAnvendt(pn, LocalDate.of(2017, 2, 11));
	}
	
	@Test
	public void anbefaletDosisPrDoegnTest() {
		// TC1
		patient.setVaegt(13);
		assertEquals(1.3, s.anbefaletDosisPrDoegn(patient, laegemiddel), 0.01);
		// TC2
		patient.setVaegt(43);
		assertEquals(6.45, s.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		// TC3
		patient.setVaegt(124);
		assertEquals(19.84, s.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void anbefaletDosisPrDoegnTestIllegal() {
		// TC4
		patient.setVaegt(0);
		s.anbefaletDosisPrDoegn(patient, laegemiddel);
	}
	
	@Test
	public void antalOrdinationerPrVaegtPrLaegemiddel1(){
		Patient p1 = s.opretPatient("012312374", "Test1", 24);
		DagligFast df1 = new DagligFast(f, LocalDate.of(2017, 3, 2), LocalDate.of(2017, 3, 5));
		p1.addOrdination(df1);
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(23, 78, f));
		
		p1.addOrdination(df1);
		assertEquals(2, s.antalOrdinationerPrVægtPrLægemiddel(23, 78, f));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void antalOrdinationerPrVaegtPrLaegemiddel2() {
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(23, 20, f));
	}
}
