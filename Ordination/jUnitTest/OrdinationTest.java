package jUnitTest;

import static org.junit.Assert.*;

import java.time.DateTimeException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;

public class OrdinationTest {
	
Laegemiddel lm1 = new Laegemiddel("Fiktivt drug", 10, 15, 20,"Stik pille");

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void opretOrdinationAntalDage1() {
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 2, 28), LocalDate.of(17, 3, 28));
		assertEquals(29, ds1.antalDage());
	}
	
	@Test
	public void opretOrdinationAntalDage2() {
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 4, 26), LocalDate.of(17, 5, 26));
		assertEquals(31, ds1.antalDage());
	}
	
	@Test(expected = DateTimeException.class)
	public void opretOrdinationAntalDageUgyldigDato() {
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 13, 26), LocalDate.of(17, 3, 26));	
	}
	
	@Test(expected = DateTimeException.class)
	public void opretOrdinationAntalDageUgyldigDato2() {
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 3, 26), LocalDate.of(17, 13, 26));
	}
	
	

}
