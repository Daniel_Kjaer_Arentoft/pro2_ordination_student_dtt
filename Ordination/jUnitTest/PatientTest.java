package jUnitTest;

import static org.junit.Assert.*;

import org.junit.Test;

import ordination.Patient;

public class PatientTest {
    
    @Test
    public void testPatient() {
        Patient patient = new Patient("1234567890", "Test Testson", 60);
        assertEquals("1234567890", patient.getCprnr());
        assertEquals("Test Testson", patient.getNavn());
        assertEquals(60, patient.getVaegt(), 0.01);
    }
    
}
