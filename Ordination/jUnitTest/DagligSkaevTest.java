package jUnitTest;

import static org.junit.Assert.*;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;

public class DagligSkaevTest {
	Laegemiddel lm1 = new Laegemiddel("Fiktivt lægemiddel",10, 15, 20, "pille");

	@Before
	public void setUp() throws Exception {
		
		
	}
	
	//Construtor

	@Test
	public void testDagligSkaevConstructor1() {
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 3, 1), LocalDate.of(17, 3, 3) );
		assertEquals(lm1, ds1.getLaegemiddel());
		assertEquals(LocalDate.of(17, 3, 1), ds1.getStartDen());
		assertEquals(LocalDate.of(17, 3, 3), ds1.getSlutDen());
	}
	
	@Test(expected = DateTimeException.class)
	public void testDagligSkaevConstructorUgyldigStartDato() {
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 13, 1), LocalDate.of(17, 3, 3) );
	}
	
	@Test(expected = DateTimeException.class)
	public void testDagligSkaevConstructorUgyldigSlutDato() {
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 3, 1), LocalDate.of(17, 13, 3) );
	}
	
	//opretDosis
	
	@Test
	public void opretDosisTest1(){
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 3, 1), LocalDate.of(17, 3, 3) );
		ds1.opretDosis(LocalTime.of(16, 00), 4);
		assertEquals(4, ds1.getDoser().get(0).getAntal(), 0.0001);
		assertEquals(LocalTime.of(16, 00), ds1.getDoser().get(0).getTid());
	}
	
	@Test
	public void opretDosisTest2(){
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 3, 1), LocalDate.of(17, 3, 3) );
		ds1.opretDosis(LocalTime.of(00, 00), 1);
		assertEquals(1, ds1.getDoser().get(0).getAntal(), 0.0001);
		assertEquals(LocalTime.of(00, 00), ds1.getDoser().get(0).getTid());
	}
	
	@Test(expected = DateTimeException.class)
	public void opretDosisTestUgyldigTid(){
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 3, 1), LocalDate.of(17, 3, 3) );
		ds1.opretDosis(LocalTime.of(26, 00), 4);
		assertEquals(4, ds1.getDoser().get(0).getAntal(), 0.0001);
		assertEquals(LocalTime.of(26, 00), ds1.getDoser().get(0).getTid());
	}
	
	//samletDosis & doegnDosis()
	
	@Test
	public void samletDosisdoegnDosis1(){
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 2, 27), LocalDate.of(17, 2, 28) );
		ds1.opretDosis(LocalTime.of(7, 00), 4);
		ds1.opretDosis(LocalTime.of(10, 00), 8);
		ds1.opretDosis(LocalTime.of(13, 00), 2);
		ds1.opretDosis(LocalTime.of(15, 00), 4);
		ds1.opretDosis(LocalTime.of(20, 00), 2);
		assertEquals(40, ds1.samletDosis(), 0.0001);
		assertEquals(20, ds1.doegnDosis(), 0.0001);
	}
	
	@Test
	public void samletDosisdoegnDosis2(){
		DagligSkaev ds1 = new DagligSkaev(lm1, LocalDate.of(17, 2, 27), LocalDate.of(17, 2, 28) );
		ds1.opretDosis(LocalTime.of(12, 00), 4);
		assertEquals(8, ds1.samletDosis(), 0.0001);
		assertEquals(4, ds1.doegnDosis(), 0.0001);
	}
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
}
