package jUnitTest;

import static org.junit.Assert.*;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;

public class DagligFastTest {

    private Laegemiddel laegemiddel1;

    @Before
    public void setUp() throws Exception {
        laegemiddel1 = new Laegemiddel("LM1", 10, 15, 20, "Pille");
    }

    //Test af Constructor
    @Test
    public void testDagligFast1() {
        DagligFast df =
            new DagligFast(laegemiddel1, LocalDate.of(2017, 02, 28), LocalDate.of(2017, 03, 05));
        assertEquals(laegemiddel1, df.getLaegemiddel());
        assertEquals(LocalDate.of(2017, 02, 28), df.getStartDen());
        assertEquals(LocalDate.of(2017, 03, 05), df.getSlutDen());
    }

    @Test(expected = DateTimeException.class)
    public void testDagligFast2() {
        DagligFast df =
            new DagligFast(laegemiddel1, LocalDate.of(2017, 02, 28), LocalDate.of(2017, 03, 05));
        assertEquals(laegemiddel1, df.getLaegemiddel());
        assertEquals(LocalDate.of(2017, 13, 28), df.getStartDen());
        assertEquals(LocalDate.of(2017, 03, 05), df.getSlutDen());
    }

    @Test(expected = DateTimeException.class)
    public void testDagligFast3() {
        DagligFast df =
            new DagligFast(laegemiddel1, LocalDate.of(2017, 02, 28), LocalDate.of(2017, 03, 05));
        assertEquals(laegemiddel1, df.getLaegemiddel());
        assertEquals(LocalDate.of(2017, 02, 28), df.getStartDen());
        assertEquals(LocalDate.of(2017, 13, 05), df.getSlutDen());
    }

    //Test af opretDosis()
    @Test
    public void testOpretDosis1() {
        DagligFast df =
            new DagligFast(laegemiddel1, LocalDate.of(2017, 02, 28), LocalDate.of(2017, 03, 05));
        df.opretDosis(LocalTime.of(16, 00), 4);
        Dosis[] testDoser = df.getDoser();
        assertEquals(LocalTime.of(16, 00), testDoser[0].getTid());
        assertEquals(4, testDoser[0].getAntal(), 0);
    }

    @Test
    public void testOpretDosis2() {
        DagligFast df =
            new DagligFast(laegemiddel1, LocalDate.of(2017, 02, 28), LocalDate.of(2017, 03, 05));
        df.opretDosis(LocalTime.of(18, 00), 1);
        Dosis[] testDoser = df.getDoser();

        assertEquals(LocalTime.of(18, 00), testDoser[0].getTid());
        assertEquals(1, testDoser[0].getAntal(), 0);
    }

    @Test(expected = DateTimeException.class)
    public void testOpretDosis3() {
        DagligFast df =
            new DagligFast(laegemiddel1, LocalDate.of(2017, 02, 28), LocalDate.of(2017, 03, 05));
        df.opretDosis(LocalTime.of(26, 00), 4);
        Dosis[] testDoser = df.getDoser();

        assertEquals(LocalTime.of(26, 00), testDoser[0].getTid());
        assertEquals(4, testDoser[0].getAntal(), 0);
    }
    
    //Test af samletDosis() og doegnDosis()
    @Test
    public void testSamletDosis1() {
        DagligFast df =
            new DagligFast(laegemiddel1, LocalDate.of(2017, 02, 27), LocalDate.of(2017, 02, 28));
        df.opretDosis(LocalTime.of(07, 00), 2);
        df.opretDosis(LocalTime.of(10, 00), 2);
        df.opretDosis(LocalTime.of(12, 00), 2);
        df.opretDosis(LocalTime.of(18, 00), 2);

        assertEquals(8, df.doegnDosis(), 0);
        assertEquals(16, df.samletDosis(), 0);
    }

    @Test
    public void testSamletDosis2() {
        DagligFast df =
            new DagligFast(laegemiddel1, LocalDate.of(2017, 02, 27), LocalDate.of(2017, 02, 28));
        df.opretDosis(LocalTime.of(9, 00), 4);
        df.opretDosis(LocalTime.of(13, 00), 2);
        df.opretDosis(LocalTime.of(15, 00), 4);
        df.opretDosis(LocalTime.of(20, 00), 6);

        assertEquals(16, df.doegnDosis(), 0);
        assertEquals(32, df.samletDosis(), 0);
    }

}
