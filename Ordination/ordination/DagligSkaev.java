package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<Dosis>();
	
	
	/**
	 * 
	 * @param laegemiddel
	 * @param startDen
	 * @param slutDen
	 * Pre: startDen is before slutDen
	 */
	
	public DagligSkaev(Laegemiddel laegemiddel, LocalDate startDen, LocalDate slutDen) {
		super(laegemiddel, startDen, slutDen);
	}
	
	/**
	 * @param tid
	 * @param antal
	 * Pre: antal => 0
	 */
    public void opretDosis(LocalTime tid, double antal) {
        Dosis dosis = new Dosis(tid, antal);
        doser.add(dosis);
    }

	@Override
	public double samletDosis() {
		return antalDage() * doegnDosis();
	}

	@Override
	public double doegnDosis() {
		int doserAntal = 0;
		for(Dosis dosis : doser) {
			doserAntal += dosis.getAntal();
		}
		return doserAntal;
	}

	@Override
	public String getType() {
		return "Daglig Skæv";
	}

	public ArrayList<Dosis> getDoser() {
		return doser;
	}
}
