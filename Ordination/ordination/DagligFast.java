package ordination;

import java.time.*;

public class DagligFast extends Ordination {
    private Dosis[] dosis;
    
    /**
     * 
     * @param laegemiddel
     * @param startDate
     * @param slutDate
     * Pre: startDate is before slutDate
     */
    
    public DagligFast(Laegemiddel laegemiddel, LocalDate startDate, LocalDate slutDate) {
        super(laegemiddel, startDate, slutDate);
        this.dosis = new Dosis[4];
    }

    /**
	 * @param tid
	 * @param antal
	 * Pre: antal => 0
	 */
    public void opretDosis(LocalTime tid, double antal) {
        Dosis dosis = new Dosis(tid, antal);
        boolean found = false;
        int i = 0;
        while (!found && i < this.dosis.length) {
            if (this.dosis[i] == null) {
                this.dosis[i] = dosis;
                found = true;
            }
            i++;
        }
    }

    public Dosis[] getDoser() {
        return this.dosis;
    }
    
    @Override
    public double samletDosis() {
        return antalDage() * doegnDosis();
    }
    
    @Override
    public double doegnDosis() {
        double doegnDosis = 0;
        for (int i = 0; i < dosis.length; i++) {
            doegnDosis += dosis[i].getAntal();
        }
        return doegnDosis;
    }
    
    @Override
    public String getType() {
        return "Daglig Fast";
    }
}