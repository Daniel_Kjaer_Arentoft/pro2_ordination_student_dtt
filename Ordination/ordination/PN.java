package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
	
	private double antalEnheder;
	private ArrayList<LocalDate> doser = new ArrayList<>();

    public PN(Laegemiddel laegemiddel, double antalEnheder, LocalDate startDen, LocalDate slutDen) {
		super(laegemiddel, startDen, slutDen);
		this.antalEnheder = antalEnheder;
	}

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {
    	if(givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen())) {
    		doser.add(givesDen);
    		return true;
    	}
        return false;   
    }

    public double doegnDosis() {
    	double result = 0;
    	if(doser.size() >= 2) {
    		result = (getAntalGangeGivet() * antalEnheder)/(ChronoUnit.DAYS.between(doser.get(0), doser.get(doser.size() - 1)) + 1);
    	} else if (doser.size() == 1){
    		result = getAntalGangeGivet() * antalEnheder;
    	} else {
    		result = 0;
    	}
    	return result;
    }


    public double samletDosis() {
    	double result = 0;
    	double doegnDosis = doegnDosis();
    	if(doegnDosis == 0) {
    		result = 0;
    	} else {
    		result = doegnDosis * antalDage();
    	}
        return result;
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     * @return
     */
    public int getAntalGangeGivet() {
        return doser.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

	@Override
	public String getType() {
		return "PN";
	}
}
