package ordination;

import java.util.ArrayList;

public class Patient {
    private String cprnr;
    private String navn;
    private double vaegt;
    private ArrayList<Ordination> ordinationer = new ArrayList<>();

    // TODO: Link til Ordination

    /**
     *
     * @param cprnr
     * @param navn
     * @param vaegt
     * precondition : vaegt kan ikke være under 0
     **/
    public Patient(String cprnr, String navn, double vaegt) {
        this.cprnr = cprnr;
        this.navn = navn;
        this.vaegt = vaegt;
    }

    public String getCprnr() {
        return cprnr;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public double getVaegt() {
        return vaegt;
    }

    public void setVaegt(double vaegt) {
        this.vaegt = vaegt;
    }
    
    public void addOrdination(Ordination o) {
        ordinationer.add(o);
        if (o.getPatient() != this) {
            o.setPatient(this);
        }
    }

    public void removeOrdination(Ordination o) {
        ordinationer.remove(o);
        if (o.getPatient() == this) {
            o.setPatient(null);
        }
    }
    
    public ArrayList<Ordination> getOrdinationer() {
        return ordinationer;
    }

    //TODO: Metoder (med specifikation) til at vedligeholde link til Ordination

    @Override
    public String toString() {
        return navn + "  " + cprnr;
    }

}
